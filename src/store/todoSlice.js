import { createSlice } from "@reduxjs/toolkit";
import { initialState } from "../todo/constant";

const todoSlice = createSlice({
  name: "todosSlice",
  initialState,
  reducers: {
    addTodo: (state, action = { type: "default", payload: {} }) => {
      state.push({
        title: action.payload,
        key: new Date().toISOString(),
      });
    },
    deleteTodo: () => {},
  },
});

export default todoSlice;
