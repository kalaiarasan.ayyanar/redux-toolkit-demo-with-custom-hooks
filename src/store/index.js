import { configureStore } from "@reduxjs/toolkit";
import todoSlice from "./todoSlice";
import inputSlice from "./inputSlice";

const store = configureStore({
  reducer: {
    todos: todoSlice.reducer,
    input: inputSlice.reducer,
  },
});

export default store;
