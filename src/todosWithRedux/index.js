import useCustomHooksWithRedux from "./customHooksWithRedux";

function TodosWithRedux() {
  const [todos, inputText, onChangeHandler, addTodo] =
    useCustomHooksWithRedux();
  return (
    <div>
      <div>
        {todos.map(({ title }, index) => (
          <p key={index}>{title}</p>
        ))}
      </div>
      <hr />
      <input onChange={onChangeHandler} value={inputText} />
      <button onClick={addTodo}>Add Todo</button>
    </div>
  );
}

export default TodosWithRedux;
