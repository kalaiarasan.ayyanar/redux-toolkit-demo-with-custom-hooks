import { useSelector, useDispatch } from "react-redux";
import inputSlice from "../store/inputSlice";
import todoSlice from "../store/todoSlice";
import { todoStateSelector } from "./utils";

function useCustomHooksWithRedux() {
  const todos = useSelector(todoStateSelector);
  const inputText = useSelector((store) => store.input);

  const dispatch = useDispatch();

  function onChangeHandler(event) {
    dispatch(inputSlice.actions.updateField(event.target.value));
  }

  function addTodo() {
    dispatch(todoSlice.actions.addTodo(inputText));
    dispatch(inputSlice.actions.updateField(""));
  }

  return [todos, inputText, onChangeHandler, addTodo];
}

export default useCustomHooksWithRedux;
