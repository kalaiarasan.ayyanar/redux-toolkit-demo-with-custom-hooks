import Todos from "./todo";
import "./App.css";
// import TodosWithRedux from "./todosWithRedux";

function App() {
  return (
    <div className="App">
      {/* <TodosWithRedux /> */}
      <Todos />
    </div>
  );
}

export default App;
