import { useState } from "react";

const useTodosHooks = (initialState = []) => {
  const [todos, setTodos] = useState(initialState);
  const [inputText, setInputText] = useState("");

  function addTodos() {
    let currentTodos = [...todos];
    currentTodos.push({ title: inputText, key: new Date().toISOString() });
    setTodos(currentTodos);
    setInputText("");
  }

  function deleteSelectedTodo(key) {
    setTodos(todos.filter(({ key: currentTodoKey }) => currentTodoKey !== key));
  }

  function onChangeHandler(event) {
    setInputText(event.target.value);
  }

  return [todos, inputText, addTodos, deleteSelectedTodo, onChangeHandler];
};

export default useTodosHooks;
