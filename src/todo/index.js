import { useCallback } from "react";
import { initialState } from "./constant";
import useTodosCustomHooks from "./customHooks";
import IndividualTodo from "./individualTodo";

export default function TodosComponent() {
  const [todos, inputText, addTodos, deleteSelectedTodo, onChangeHandler] =
    useTodosCustomHooks(initialState);
  const updatedDelete = useCallback(
    (id) => {
      deleteSelectedTodo(id);
    },
    [deleteSelectedTodo]
  );

  return (
    <div>
      <div>
        {todos.map(({ title, key }) => (
          <IndividualTodo
            deleteSelectedTodo={updatedDelete}
            title={title}
            todoKey={key}
            key={key}
          />
        ))}
      </div>
      <hr />
      <input type={"text"} onChange={onChangeHandler} value={inputText} />
      <button onClick={addTodos}> Add todo </button>
    </div>
  );
}
