export const initialState = [
  {
    title: "Learn",
    key: "todo-1",
  },
  {
    title: "Practice",
    key: "todo-2",
  },
  {
    title: "Get Job",
    key: "todo-3",
  },
  {
    title: "Go to UK",
    key: "todo-4",
  },
];
