export default function IndividualTodo({ deleteSelectedTodo, todoKey, title }) {
  console.log("rendering - ", todoKey);
  return (
    <div>
      <p>{title}</p>
      <button onClick={() => deleteSelectedTodo(todoKey)}>Delete</button>
    </div>
  );
}
